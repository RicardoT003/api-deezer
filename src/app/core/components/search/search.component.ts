import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, tap } from 'rxjs/operators';
import { ArtistService } from '../../services/artist.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit {
  searchForm: FormGroup;
  constructor(public fb: FormBuilder, private artistService: ArtistService) {
    this.searchForm = this.fb.group({
      search: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.searchForm
      .get('search')
      .valueChanges.pipe(
        debounceTime(500),
        tap((_) => {
          this._applySearch();
        })
      )
      .subscribe();
  }

  _applySearch() {
    const { search } = this.searchForm.value;
    this.artistService.searchByKeyword(search);
  }
}
