import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { SearchQuery } from 'src/app/core/store/search.query';
import { SearchStore } from 'src/app/core/store/search.store';

@Component({
  selector: 'app-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecentComponent implements OnInit {
  constructor(
    public searchQuery: SearchQuery,
    private searchStore: SearchStore
  ) {}

  ngOnInit(): void {}
  updateItemSelected(id) {
    this.searchStore.update((state) => {
      return {
        ...state,
        resultsID: id,
        isNewCancion: true,
      };
    });
  }
}
