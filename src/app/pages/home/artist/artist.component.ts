import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ArtistService } from 'src/app/core/services/artist.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArtistComponent implements OnInit {
  constructor(private artistService: ArtistService) {}

  ngOnInit(): void {
    this.artistService.getArtist();
  }
}
