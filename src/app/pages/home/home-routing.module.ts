import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumComponent } from './album/album.component';
import { ArtistComponent } from './artist/artist.component';

import { HomeComponent } from './home.component';
import { RecentComponent } from './recent/recent.component';
import { SongComponent } from './song/song.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'albums',
        component: AlbumComponent,
      },
      {
        path: 'artist',
        component: ArtistComponent,
      },
      {
        path: '',
        component: RecentComponent,
      },
      {
        path: 'songs',
        component: SongComponent,
      },
      {
        path: '',
        redirectTo: '/recent',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
