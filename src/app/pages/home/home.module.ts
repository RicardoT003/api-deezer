import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ArtistComponent } from './artist/artist.component';
import { AlbumComponent } from './album/album.component';
import { RecentComponent } from './recent/recent.component';
import { SongComponent } from './song/song.component';
import { ItemSelectedComponent } from './item-selected/item-selected.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomeComponent,
    ArtistComponent,
    AlbumComponent,
    RecentComponent,
    SongComponent,
    ItemSelectedComponent,
  ],
  imports: [CommonModule, HomeRoutingModule, ReactiveFormsModule],
})
export class HomeModule {}
